# K FINDER

Find dissociation constants of supramulecolar complex with two guest in competition for the same host (e.g. a crown ether or a cyclodextrin with two ions in competition like Na+ and Ca++).

![k-finder-intro](k-finder-intro.png)

Data required to set the calculation:

- Start conc. of `host I`
- Start conc. of `guest`
- Dissociation constant of `host I` - `guest`
- Rel. % of `guest`

Data required for each point in titration:

- Start conc. of `host II`
- Rel. % of `host II`

The software gives a dynamic plot as result for each point:

![k-finder-plot](k-finder-plot.png)

TODOS:

- Support for other guests
- Support for english language 