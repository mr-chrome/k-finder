// Disable warning error in the console
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

// Declare vars
const electron = require('electron');
const url = require('url');
const path = require('path');
const { app, BrowserWindow, Menu } = electron;

// Riferimento globale all'oggetto mainWindow
let mainWindow
let addWindow

function createWindow () {
  // Funzione per creare la finestra principale
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 800,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // Load index.html
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Build Menu from Template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
  // Insert Menu
  Menu.setApplicationMenu(mainMenu);
  mainWindow.setMenuBarVisibility(true)

  // Chiudi tutto quando la finestra principale viene chiusa.
  mainWindow.on('closed', () => {
    app.quit();
  })
}


// Handle create add window TODO

function createAddWindow(){
  // Funzione per creare la finestra
  addWindow = new BrowserWindow({
    width: 200,
    height: 300,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // Carica pagina
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'subindex.html'),
    protocol: 'file:',
    slashes: true
  }))

  addWindow.on('closed', () => {
    addWindow = null
  })
}

// Fase post-inizializziazione (fin qui sono state solo create le finestre)
// Alcune API possono essere utilizzate solo dopo che si verifica questo evento.
app.on('ready', createWindow, createAddWindow )

// Terminiamo l'App quando tutte le finestre vengono chiuse.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

// Menu Template (Just an array)
const mainMenuTemplate = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Add Item',
        click(){
          // Function to open another window
          createAddWindow();
        }
      },
      {
        label: 'Save Dataset',
        click(){
          // Function to save dataset
          // save();
        }
      },
      {
        label: 'Quit',
        // Add shortcut for quitting
        // If you're on a Mac, you're gonna see 'darwin' and run "Command+Q"
        accelerator: process.platform == 'darwin' ? 'Command+Q': 'Ctrl+Q',
        click(){
          app.quit();
        }
      }
    ]
  }
]

// If mac, add empty object as first object in menu
if (process.platform == 'darwin') {
  mainMenuTemplate.unshift({});
}

// If we're in production, we want developer tools in the menu
if (process.env.NODE_ENV !== 'production') {
  mainMenuTemplate.push({
    label: 'Developer Tools',
    submenu: [
      {
        label: 'Toggle DevTools',
        accelerator: process.platform == 'darwin' ? 'Command+I':
        'Ctrl+I',
        click(item, focusedWindow){
          focusedWindow.toggleDevTools();
        }
      },
      {
        role: 'reload'
      }
    ]
  });
}
