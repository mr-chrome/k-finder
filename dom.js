// MANIPULATION OF DOM

function inputGen(id, containerid, labelstring) {
  // Make outer div
  let div = document.createElement("div");
  div.setAttribute('class', 'inline_input');

  // Make label
  let lbl = document.createElement("label");
  lbl.setAttribute('class', 'form-label');
  lbl.setAttribute('for', String(id));
  lbl.innerText = labelstring;

  // Make input
  let input = document.createElement("input");
  input.setAttribute('class', 'form-input');
  input.setAttribute('id', String(id));
  input.setAttribute('type', 'text');

  // Append to div
  div.appendChild(lbl);
  div.appendChild(input);

  if (typeof containerid == 'string') {
    // Put in document
    let container = document.getElementById(containerid);
    container.appendChild(div);
  }
  else{
    return(div);
  }
}

function accordionGen(id, head, body, containerid) {
  // id = accordion id;
  // head = label text, if text; otherwise a HTML element (es. toast);
  // body = HTML element with body of accordion (es. list);
  // container = if string: id of HTML container; else HTML element;

  let div = document.createElement("div");
  div.setAttribute('class', 'accordion');

  let input = document.createElement("input");
  input.setAttribute('type', 'checkbox');
  input.setAttribute('id', id);
  input.setAttribute('name', 'accordion-checkbox');
  input.hidden = true;

  div.appendChild(input);

  if (typeof head == "string") {
    let label = document.createElement("label");
    label.setAttribute('class', 'accodion-header');
    label.setAttribute('for', id);
    label.innerText = head;

    div.appendChild(label);
  }
  else if (head instanceof HTMLElement) {
    div.appendChild(head);
  }
  else {
    console.log('accordion error!');
  }

  let arrow = document.createElement("i");
  arrow.setAttribute('class', 'fas fa-chevron-right');
  // label.appendChild(arrow);

  let divbody = document.createElement("div");
  divbody.setAttribute('class', 'accordion-body');
  div.appendChild(divbody);

  if (body instanceof HTMLElement) {
    divbody.appendChild(body);
  }
  if (typeof containerid == 'string') {
    // Put in document
    let container = document.getElementById(containerid);
    container.appendChild(div);
  }
  else{
    return(div);
  }
}

function toastGen(id, containerid, text) {
  let labeltoast = document.createElement("label");
  labeltoast.setAttribute('class', 'toast');
  labeltoast.setAttribute('id', id);

  let button = document.createElement("button");
  button.setAttribute('class', 'btn btn-clear float-right');
  labeltoast.appendChild(button);

  let content = document.createTextNode(String(text));
  labeltoast.appendChild(content);

  return(labeltoast);

  // Put in document
  // let container = document.getElementById(containerid);
  // container.appendChild(labeltoast);
}

function newSpanout(stringa, valore) { // Al momento pressoché inutile.
  let tempspan = document.createElement("span");
  tempspan.setAttribute('class', 'spanout');
  tempspan.appendChild(document.createTextNode(stringa + String(valore) + " "));
  return(tempspan);
}

function newParout(stringa, valore) { // Da sostituire 
  let tempar = document.createElement("p");
  tempar.setAttribute('class', 'parout');
  tempar.appendChild(document.createTextNode(stringa + String(valore) + " "));
  return(tempar);
}

function newDivider(container) {
  // Add divider
  var divider = document.createElement("div");
  divider.setAttribute('class', 'divider');
  container.appendChild(divider);
}

function eraseOutput(container) { // Delete all children in container
  while (container.firstChild) {
    container.firstChild.remove();
  }
}

function onEnterClick(element, button) {
  // Execute a function when the user releases a key on the keyboard
  element.addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
      // Cancel the default action, if needed
      event.preventDefault();
      // Trigger the button element with a click
      button.click();
    }
  });
}
