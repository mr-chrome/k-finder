function showLine(){
  eraseOutput(document.getElementById('graph'));
  var linevalues = document.getElementById("regression-outputs");

  // Get Data
  var mydata = [];

  for (var i = 0; i < data.points.length; i++) {
    let newobj = new Object;
    newobj.y =parseFloat(data.points[i]["complex_new_on_ref"]);
    newobj.x = parseFloat(data.points[i]["free_new_on_ref"]);
    mydata.push(newobj);
   }

  console.log(mydata);
  // var maxy = d3.max(mydata, function(d) { return d.y; });
  var maxx = d3.max(mydata, function(d) { return d.x; });

  // Line
  let linearRegression = d3.regressionLinear()
    .x(d => d.x)
    .y(d => d.y)
    .domain([0, maxx]); // Ascisse

  let res = linearRegression(mydata);

  // Print output
  eraseOutput(document.getElementById('regression-outputs'));
  var aterm = newParout("Pendenza: ", linearRegression(mydata).a.toFixed(4));
  var bterm = newParout("Intercetta: ", linearRegression(mydata).b.toFixed(4));
  var rquadro = newParout("R^2: ", linearRegression(mydata).rSquared.toFixed(4));
  var costante = newParout("COSTANTE: ", (data.cost_sp_I * linearRegression(mydata).a.toFixed(4)));
  linevalues.appendChild(aterm);
  linevalues.appendChild(bterm);
  linevalues.appendChild(rquadro);
  linevalues.appendChild(costante);

  // Print plot

    var margin = {
      top: 30,
      right: 20,
      bottom: 30,
      left: 50
      };

      width = 600 - margin.left - margin.right,
      height = 270 - margin.top - margin.bottom;

    let x = d3.scaleLinear().range([0, width]);
    let y = d3.scaleLinear().range([height, 0]);

    let svg = d3.select("#graph")
      .append("svg")
      .attr("viewBox", [
      0,
      0,
      width + margin.right + margin.left, // Larghezza finale effettiva
      height + margin.top + margin.bottom // Altezza finale effettiva
      ])
      .append("svg:g")
      .attr("transform", "translate(" + margin.right + "," + margin.top + ")");

    x.domain(d3.extent(mydata, (d) => d.x));
    y.domain(d3.extent(mydata, (d) => d.y));

    // Add the grey background that makes ggplot2 famous
    svg
      .append("rect")
        .attr("x",0)
        .attr("y",0)
        .attr("height", height)
        .attr("width", width)
        .style("fill", "EBEBEB");

    // X Axis
    svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      // Simple axis
      //.call(d3.axisBottom(x))
      // ggplot version
      .call(d3.axisBottom(x).tickSize(-height*1.3).ticks(10))
      .select(".domain").remove();

    svg.append("g")
      // Simple axis
      // .call(d3.axisLeft(y))
      // ggplot version
      .call(d3.axisLeft(y).tickSize(-width*1.3).ticks(7))
      .select(".domain").remove();

    // Grid customization in ggplot white style
    svg.selectAll(".tick line").attr("stroke", "white")

    // Make Line
    let line = d3.line()
      .x((d) => x(d[0]))
      .y((d) => y(d[1]));

    svg.append("path")
      .datum(res)
      .attr("d", line)
      .classed("regression", true)

    // Make dots
    var color = d3.scaleOrdinal()
    .domain(["setosa", "versicolor", "virginica" ])
    .range([ "#F8766D", "#00BA38", "#619CFF"]);

    // Add the valueline path.
    svg.selectAll("circle")
      .data(mydata)
      .enter()
      .append("circle")
      .attr("r", 5)
      .attr("cx", (d) => x(d.x))
      .attr("cy", (d) => y(d.y))
      .style("fill", function (d) { return color(d.Species) } );

}
