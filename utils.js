//TODO: dare nome alle specie (I e II, ref e new) per l'interfaccia via form iniziale
//TODO: Sistemare layout output usando i tiles (espandibili ad accordion)
//DONE: generare e i form di index.html attraverso js con poche funzioni e dichiarare contemporaneamente le variabili per ognuno
//TODO--> Ne consegue la possibilità di scegliere quali form mostrare, quindi anche semplice "conc. assoluta mode"

//TODO: salvare e caricare i dataset (csv?)
//TODO: integrare Mathjax e mostrare equazione della retta
//TODO: inserire possibilità di plottare non linearmente
//TODO: inserire possiblità di variare lo stile del grafico
//TODO: estendere il dominio del grafico di un margine positivo e negativo proporzionale alle dimensioni

// CALCULATION FUNCTIONS

function concass(conc_in, perc_in_I, perc_in) { // calcolo concentrazione assoluta
  var result = (conc_in * perc_in) / (perc_in_I + perc_in);
  return(parseFloat(result.toFixed(2)));
}

// Define Object
var data = new Object();
function inizializza() {
    data.conc_in_CE = parseFloat(input_conc_in.value); // Concentrazione iniziale macrociclo CE
    data.conc_in_sp_ref = parseFloat(input_conc_in_sp_I.value); // Conc. iniziale specie I (ref)
    data.perc_in_ref = parseFloat(input_perc_in_sp_I.value); // Percentuale iniziale relativa specie I (ref)
    data.cost_sp_I = parseFloat(input_costante_sp_I.value); // Costante della specie I (ref)
    data.points = []; // Lista di oggetti con i valori dei punti inseriti

    if (typeof data.conc_in_CE != 'undefined' && data.conc_in_CE != '' && typeof data.perc_in_ref != 'undefined' && data.perc_in_ref != '') {
      button_inizializza.disabled = true;
      input_perc_in_sp_II.disabled = false;
      input_conc_in_sp_II.disabled = false;
      button_calcola.disabled = false;
      button_annulla.disabled = false;
      button_line.disabled = false;
    }
    else {
      alert("Dati insufficienti per l'inizializzazione!");
    }

    // Enable enter button on button_calcola
    onEnterClick(input_conc_in_sp_II, button_calcola);
    onEnterClick(input_perc_in_sp_II, button_calcola);
}

function getDataPoint() {
  var point = new Object(); // Make new object

  point.conc_in_sp_new = parseFloat(input_conc_in_sp_II.value); // Concentrazione di specie II aggiunta
  point.perc_in_sp_new = parseFloat(input_perc_in_sp_II.value); // Percentuale rel. complesso CE@SpecieII

  point.conc_ass_complex_new = concass(data.conc_in_CE, data.perc_in_ref, point.perc_in_sp_new); // Conc. assoluta complesso CE@SpecieII (new)
  point.conc_ass_complex_ref = data.conc_in_CE - point.conc_ass_complex_new; // Conc. assoluta complesso CE@Specie I (ref)
  point.conc_ass_free_ref =  data.conc_in_sp_ref - point.conc_ass_complex_ref; // Conc. assoluta specie I (ref) non complessata
  point.conc_ass_free_new = point.conc_in_sp_new - point.conc_ass_complex_new; // Conc. assoluta specie II (new) non complessata

  point.complex_new_on_ref = point.conc_ass_complex_new / point.conc_ass_complex_ref; // Rapporto tra conc. assolute delle due specie complessate
  point.free_new_on_ref = point.conc_ass_free_new / point.conc_ass_free_ref; // Rapporto tra concentrazioni assolute delle due specie non complessate
  return(point);
}

function calcola() {
  data.points.push(getDataPoint());
  displayOutput();
  if(data.points.length>1){
    showLine();
  }
}

// Display Output function
function displayOutput() {
  // Get the print zone
  var print = document.getElementById("outputs");
  var point_qt = data.points.length; // Quantità di punti raccolti
  var last_point = data.points[point_qt - 1]; // Attingi all'ultimo punto raccolto

  // Display objects
  //TODO: elimina valore ed eguaglialo al nome. Poi usa il nome per pescare da last_point
  d_obj = [{
    nome: 'rel_II',
    valore: last_point.perc_in_sp_new,
    lbl: "Percentuale rel. complesso CE@SpecieII (new): "
  },{
    nome: 'concass_free_I',
    valore: last_point.conc_ass_free_ref,
    lbl: "Conc. assoluta specie I (ref) non complessata: "
  },{
    nome: 'concass_free_II',
    valore: last_point.conc_ass_free_new,
    lbl: "Conc. assoluta specie II (new) non complessata: "
  },{
    nome: 'concass_complex_I',
    valore: last_point.conc_ass_complex_ref,
    lbl: "Conc. assoluta complesso CE@SpecieI (ref): "
  },{
    nome: 'concass_complex_II',
    valore: last_point.conc_ass_complex_new,
    lbl: "Conc. assoluta complesso CE@SpecieII (new): "
  },{
    nome: 'ratio_free_I_II',
    valore: last_point.free_new_on_ref,
    lbl: "Rapporto tra concentrazioni assolute delle due specie non complessate (new on ref): "
  }
  ]

  let newid = 'd-obj-' + String(point_qt);
  var thispars = document.createElement("div");
  thispars.setAttribute('style', 'display: grid;');

  for (var i = 0; i < d_obj.length; i++) {
    let propid = newid + '-' + String(d_obj[i].nome);

    if (d_obj[i]['nome'] == 'rel_II') {
      var thistoast = toastGen(propid, undefined, (d_obj[i].lbl + String(d_obj[i].valore)));
      thistoast.setAttribute('class', 'accordion-header toast');
      thistoast.setAttribute('for', newid);
    }
    else{
      let thisinp = inputGen(propid, undefined, d_obj[i].lbl); // è un div
      let innerinput = thisinp.querySelector('input'); // Get inner input of div
      innerinput.value = d_obj[i].valore; // Set value 
      innerinput.readOnly = true; // Set not editable
      thispars.appendChild(thisinp);
    }
  }

  accordionGen(newid, thistoast, thispars, 'outputs');
  newDivider(print);
}

function annulla() {
  if (data.points.length > 0) {
    data.points.pop();
  }
  if(data.points.length>1){
    showLine();
  }
}

function save(){
  var fs = require('fs');
  try { fs.writeFileSync('myfile.json', JSON.stringify(data), 'utf-8'); }
  catch(e) { alert('Failed to save the file !'); }
}
